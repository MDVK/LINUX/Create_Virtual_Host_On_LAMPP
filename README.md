# How to setup a Virtual Host locally with XAMPP in LINUX

---

## 1. Allow the usage of custom virtual hosts

By default, xampp in ubuntu won't use the`httpd-vhosts.conf`file \(the location of the virtual hosts\), therefore we need to indicate that this file will be included during the runtime of apache. Open with your favorite code editor the`httpd.conf`file located tipically in`/opt/lampp/etc`or just execute the following command in your terminal to open a simple editor:

> sudo subl3 /opt/lampp/etc/httpd.conf



Now locate yourself in \(about\) the line **487 **where you probably will find the following lines:

> **`Include etc/extra/httpd-vhosts.conf`**



## 2. Create a custom domain in the hosts file of your system

> sudo subl3` /etc/hosts`



> `# Static table lookup for hostnames.`
>
> `# See hosts(5) for details.`
>
> `127.0.0.1       localhost`
>
> `127.0.1.1       mdvk`
>
> `127.0.0.5       dev.mdvk`



## 3. Create your first virtual host

Tipically, you need to create the virtual host in the`httpd-vhosts.conf`file located in `/opt/lampp/etc/extra`. Use your favorite editor to edit that file or just execute the following command to edit it in a terminal:



> ```
> sudo subl3 /opt/lampp/etc/extra/httpd-vhosts.conf
> ```

And create your own virtual host in this file. As shown in our custom domain in the vhost file of the system, the port that we are going to use is

`127.0.0.5`

, therefore our virtual host will be:

> \#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#
>
> \# MAIN PROJECT ON HERE 
>
>
>
> &lt;VirtualHost 127.0.0.5:80&gt;
>
> \# /opt/lampp/htdocs/school
>
>   DocumentRoot "/opt/lampp/htdocs/school/"
>
>   DirectoryIndex index.php
>
>
>
>   &lt;Directory "/opt/lampp/htdocs/school/"&gt;
>
>     Options All
>
>     AllowOverride All
>
>     Require all granted
>
>   &lt;/Directory&gt;
>
> &lt;/VirtualHost&gt;



## 4. Test your virtual host

To test it, in the folder`/opt/lampp/htdocs/my-first-project`, create a simple PHP file \(`index.php`\) that will contain the following PHP code:

> `sudo /opt/lampp/lampp start`



